import INavLink from "./NavLink/types";
import { v4 as uuidv4 } from "uuid";
import { Colors } from "../../../global-styles";

export const generalLinksData: INavLink[] = [
    {
        id: uuidv4(),
        name: "O nas",
        slug: "/o-nas",
        hovercolor: "lightGray",
    },
    {
        id: uuidv4(),
        name: "Polityka prywatności",
        slug: "/polityka-prywatnosci",
        hovercolor: "lightGray",
    },
];

export const navLinksData: INavLink[] = [
    {
        id: uuidv4(),
        name: "Wszystko",
        slug: "/",
        hovercolor: Colors._orange02,
    },
    {
        id: uuidv4(),
        name: "Nauka",
        slug: "/science",
        hovercolor: Colors._blue01,
    },
    {
        id: uuidv4(),
        name: "Śmieszne",
        slug: "/funny",
        hovercolor: Colors._yellow01,
    },
    {
        id: uuidv4(),
        name: "Polityka",
        slug: "/politics",
        hovercolor: Colors._red01,
    },
    {
        id: uuidv4(),
        name: "Muzyka",
        slug: "/music",
        hovercolor: Colors._lilac01,
    },
    {
        id: uuidv4(),
        name: "Film",
        slug: "/film",
        hovercolor: Colors._violet01,
    },
    {
        id: uuidv4(),
        name: "Sport",
        slug: "/sport",
        hovercolor: Colors._orange02,
    },
    {
        id: uuidv4(),
        name: "Ekonomia",
        slug: "/economy",
        hovercolor: Colors._sea01,
    },
    {
        id: uuidv4(),
        name: "Historia",
        slug: "/history",
        hovercolor: Colors._gray05,
    },
    {
        id: uuidv4(),
        name: "Cytaty",
        slug: "/quotes",
        hovercolor: Colors._brown01,
    },
];
