import styled, { css } from "styled-components";
import { IStyledButton, ButtonVariant } from "./types";
import {
    Colors,
    FontSizes,
    FontWeights,
    FontFamilies,
} from "../../../global-styles";

const variantStyles = (variant: ButtonVariant = "default") =>
    ({
        default: css`
            border: 1px solid transparent;
        `,
        outlined: css`
            border: 1px solid ${Colors._gray05};
            :hover {
                background-color: ${Colors._gray05};
            }
        `,
    }[variant]);

export const StyledButton = styled.button<IStyledButton>`
    ${({ variant }) => variantStyles(variant)}

    position: relative;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

    color: ${Colors._orange02};
    font-family: ${FontFamilies._oswaldRegular};
    font-size: ${FontSizes._px15};
    font-weight: ${FontWeights._regular};

    background-color: transparent;
    border-radius: 3px;
    padding: 3px;
    transition: 0.2s;
    height: 26px;

    :hover {
        cursor: pointer;
    }
`;
